//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.10.29 at 08:12:53 PM CET 
//


package pl.suvsoft;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FilmT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilmT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filmTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filmYear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="filmDuration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="filmDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilmT", propOrder = {
    "filmTitle",
    "filmYear",
    "filmDuration",
    "filmDescription"
})
public class FilmT {

    @XmlElement(required = true)
    protected String filmTitle;
    protected int filmYear;
    @XmlElement(required = true)
    protected String filmDuration;
    @XmlElement(required = true)
    protected String filmDescription;

    /**
     * Gets the value of the filmTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilmTitle() {
        return filmTitle;
    }

    /**
     * Sets the value of the filmTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilmTitle(String value) {
        this.filmTitle = value;
    }

    /**
     * Gets the value of the filmYear property.
     * 
     */
    public int getFilmYear() {
        return filmYear;
    }

    /**
     * Sets the value of the filmYear property.
     * 
     */
    public void setFilmYear(int value) {
        this.filmYear = value;
    }

    /**
     * Gets the value of the filmDuration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilmDuration() {
        return filmDuration;
    }

    /**
     * Sets the value of the filmDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilmDuration(String value) {
        this.filmDuration = value;
    }

    /**
     * Gets the value of the filmDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilmDescription() {
        return filmDescription;
    }

    /**
     * Sets the value of the filmDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilmDescription(String value) {
        this.filmDescription = value;
    }

}
