package pl.suvsoft;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws JAXBException, IOException {
        System.out.println("elo");
        Document document = null;
        String httpAddress = args[0];
        try {
            if (args.length > 0){
                document = Jsoup.connect(httpAddress).get();
            } else {
                System.out.println("Please, give an http address");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = document.select(".filmPreview");
        boolean hasNextPage = false;
        if(document.select("#searchPaginator > .paginator_cont > ul > li > span > .nextLink").first().childNode(0) != null){
            hasNextPage = true;
        }

        System.out.println("Conversion in progress");

        int i = 1;
        ObjectFactory objectFactory = new ObjectFactory();
        FilmInfoT films = objectFactory.createFilmInfoT();

        int pageNumber = 1;
        int limit = 20;
        while(hasNextPage && pageNumber <= limit) {
            System.out.println("Page number: " + pageNumber);
            elements = document.select(".filmPreview");
            for (Element element : elements) {
                System.out.println("Convert film number: " + i);

                FilmT film = objectFactory.createFilmT();
                film.setFilmTitle(element.select("h3 > a").first().childNode(0).toString());
                String year = element.select("h3 > span").first().childNode(0).toString();
                film.setFilmYear(Integer.valueOf(year.substring(1, 5)));
                film.setFilmDuration(element.select(".filmTime").first().childNode(0).toString().trim());
                film.setFilmDescription(element.select(".relative > .filmPlot > p").first().childNode(0).toString());

                films.getFilm().add(film);
                i++;
            }
            if(document.select("#searchPaginator > .paginator_cont > ul > li > span > .nextLink").first().childNode(0) != null){
                hasNextPage = true;
                pageNumber++;
                document = Jsoup.connect(httpAddress.concat("?page=" + pageNumber)).get();
            } else {
                hasNextPage = false;
            }
        }

        FilmsInfoXmlT filmsInfoXmlT = objectFactory.createFilmsInfoXmlT();
        filmsInfoXmlT.setFilms(films);

        JAXBContext context = JAXBContext.newInstance("pl.suvsoft");
        JAXBElement<FilmsInfoXmlT> element = objectFactory.createFilmsInfoXml(filmsInfoXmlT);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);

        File file;
        String pathToFile = args[1];
        if (args[1] == null) {
            file = new File("D:\\GIT\\FunApp\\result.xml");
        } else {
            file = new File(pathToFile);
        }

        marshaller.marshal(element, file);

        System.out.println("Conversion completed");

        System.out.println("elo");
    }
}
